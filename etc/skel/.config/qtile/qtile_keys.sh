#!/usr/bin/env bash

sed -n '/START_KEYS/,/END_KEYS/p' ~/.config/qtile/config.py | \
    grep 'Key' | \
    grep -v '# ' | \
    sed -e 's/^[ /t]*//' | \
    yad --text-info
